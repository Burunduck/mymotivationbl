//
// Created by burunduck on 10.11.2020.
//

#ifndef MYBD_WRAPPERDB_H
#define MYBD_WRAPPERDB_H
#include <iostream>
#include <map>

using mapApi = std::map<std::string, std::string>;

enum Model{
    USER,
    GOAL,
    REPORT,
    RECORD,
    MODERATOR,
};

class WrapperDb {
public:
    WrapperDb();
    WrapperDb( Model obj, mapApi parameters );
    void setWrap( Model obj, mapApi parameters );
    Model getModel();
    mapApi getParameters();

private:
    Model _model;
    mapApi _parameters;
};


#endif //MYBD_WRAPPERDB_H