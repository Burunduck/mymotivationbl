//
// Created by burunduck on 10.11.2020.
//

#ifndef BL_ICONTROLLER_H
#define BL_ICONTROLLER_H

#include <map>
#include "WrapperDb.h"
#include "IDb.h"

using mapApi = std::map<std::string, std::string>;

class IController {
protected:
    IDb* proxyDb;
public:
    virtual ~IController();
    IController();
    IController(IDb* proxyDb);
    virtual mapApi execute(mapApi);
};


#endif //BL_ICONTROLLER_H
