//
// Created by burunduck on 10.11.2020.
//

#ifndef BL_BL_H
#define BL_BL_H
#include <string>
#include <map>
#include "IController.h"
#include "IValidator.h"


using mapApi = std::map<std::string, std::string>;

class BL {
private:
    IValidator* validator;
    IDb* proxyDb;
    IController* controller;
    void ChooseControllers(const std::string& c);
public:
    BL();
    int SetController(IController*);
    int SetValidator(IValidator*);
    int SetState(std::string command);
    mapApi Execute(mapApi);
};


#endif //BL_BL_H
