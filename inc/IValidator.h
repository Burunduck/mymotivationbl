//
// Created by burunduck on 12.11.2020.
//

#ifndef BL_IVALIDATOR_H
#define BL_IVALIDATOR_H
#include <map>

using mapApi = std::map<std::string, std::string>;
class IValidator {
public:
    virtual ~IValidator();
    virtual mapApi Handle(mapApi);
};


#endif //BL_IVALIDATOR_H
