//
// Created by burunduck on 12.11.2020.
//

#ifndef BL_IDB_H
#define BL_IDB_H

class IDb {
public:
    virtual ~IDb();
    virtual int Create();
    virtual int Update();
    virtual int Delete();
    virtual int Select();
};


#endif //BL_IDB_H
