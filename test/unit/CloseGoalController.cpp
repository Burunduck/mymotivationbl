#include "gtest/gtest.h"
#include "CloseGoalController.h"


//CloseGoalController(IDb* db);
//mapApi execute(mapApi) override;

using mapApi = std::map<std::string, std::string>;

TEST(close_goal, no_goal) {
    mapApi request {{"goal_id", "fdfdf"}};
    CloseGoalController* cl = new CloseGoalController(new IDb);
    mapApi response = cl->execute(request);
    ASSERT_EQ(response.find("status")->second, "false");
    ASSERT_EQ(response.find("add_each")->second, "0");
}

TEST(close_goal, have_goal) {
    mapApi request {{"goal_id", "1"}};
    CloseGoalController* cl = new CloseGoalController(new IDb);
    mapApi response = cl->execute(request);
    ASSERT_EQ(response.find("status")->second, "true");
    ASSERT_NE(response.find("users_id")->second, "2");
    ASSERT_NE(response.find("add_each")->second, "0");
}

TEST(close_goal, no_mans_stay) {
    mapApi request {{"goal_id", "1"}};
    CloseGoalController* cl = new CloseGoalController(new IDb);
    mapApi response = cl->execute(request);
    ASSERT_EQ(response.find("status")->second, "true");
    ASSERT_EQ(response.find("users_id")->second, "0");
    ASSERT_EQ(response.find("add_each")->second, "0");
}