cmake_minimum_required(VERSION 3.17)

file(GLOB sources ${PROJECT_SOURCE_DIR}/src/*.cpp  ${PROJECT_SOURCE_DIR}/Models/src/*.cpp ${PROJECT_SOURCE_DIR}/Controlers/src/*.cpp)
include_directories(${PROJECT_SOURCE_DIR}/inc ${PROJECT_SOURCE_DIR}/Models/inc ${PROJECT_SOURCE_DIR}/Controlers/inc)

file(GLOB tests ${PROJECT_SOURCE_DIR}/test/unit/BL.cpp GLOB tests ${PROJECT_SOURCE_DIR}/test/unit/CloseGoalController.cpp
        GoalCreateController.cpp RegisterController.cpp SendReportController.cpp)

#list(REMOVE_ITEM tests main.cpp)
#message("${tests}")

foreach(file ${tests})
    set(name)
    get_filename_component(name ${file} NAME_WE)
    add_executable("${name}_tests"
            ${sources}
            ${file}
            "main.cpp")
    target_link_libraries("${name}_tests" gtest_main)
    add_test(NAME ${name} COMMAND "${name}_tests")
endforeach()