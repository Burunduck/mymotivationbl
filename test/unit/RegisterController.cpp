#include "gtest/gtest.h"

#include "RegisterController.h"

//GoalCreateController(IDb* db);
//mapApi execute(mapApi) override;

using mapApi = std::map<std::string, std::string>;

TEST(register_controller, register_test_no_name) {
    mapApi request {{"name", ""},
                    {"lastname", "Alesha"},
                    {"password", "12345678"}};
    RegisterController* rg = new RegisterController();
    mapApi response = rg->execute(request);
    ASSERT_EQ(response.find("error")->second, "true");
    ASSERT_EQ(response.find("status")->second, "bad_name");
}


TEST(register_controller, register_test_no_last_name) {
    mapApi request {{"name", "Dima"},
                    {"lastname", ""},
                    {"password", "12345678"}};
    RegisterController* rg = new RegisterController();
    mapApi response = rg->execute(request);
    ASSERT_EQ(response.find("error")->second, "true");
    ASSERT_EQ(response.find("status")->second, "bad_last_name");
}

TEST(register_controller, register_test) {
    mapApi request {{"name", "Dima"},
                    {"lastname", "Alesha"},
                    {"password", "12345"}};
    RegisterController* rg = new RegisterController();
    mapApi response = rg->execute(request);
    ASSERT_EQ(response.find("error")->second, "false");
    ASSERT_EQ(response.find("status")->second, "ok");
    ASSERT_NE(atoi(response.find("user_id")->second.c_str()), 0);
}


TEST(register_controller, register_test_small_pass) {
    mapApi request {{"name", "Dima"},
                    {"lastname", "Alesha"},
                    {"password", "123"}};
    RegisterController* rg = new RegisterController();
    mapApi response = rg->execute(request);
    ASSERT_NE(response.find("error")->second, "true");
    ASSERT_NE(response.find("status")->second, "small_pass");
}

TEST(register_controller, hash) {
    mapApi request {{"name", "Dima"},
                    {"lastname", "Alesha"},
                    {"password", "12345678"}};
    RegisterController* rg = new RegisterController();
    int hash = rg->HashPassword(request.find("password")->second);
    int other_hash = rg->HashPassword("123345678");
    ASSERT_NE(hash, other_hash);
    ASSERT_EQ(hash, other_hash);
}