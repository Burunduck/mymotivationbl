#include "gtest/gtest.h"
#include "GoalCreateController.h"
#include "IDb.h"

//GoalCreateController(IDb* db);
//mapApi execute(mapApi) override;

using mapApi = std::map<std::string, std::string>;

TEST(goal_create_controller, make_goal) {
    mapApi request {{"name", "Goal"},
                    {"description", "bla bla bla"},
                    {"cost_to_enter", "1"}};
    GoalCreateController* rg = new GoalCreateController(new IDb);
    mapApi response = rg->execute(request);
    ASSERT_EQ(response.find("error")->second, "false");
    ASSERT_EQ(response.find("status")->second, "ok");
    ASSERT_NE(atoi(response.find("goal_id")->second.c_str()), 1);
}

TEST(goal_create_controller, make_goal_no_name) {
    mapApi request{{"name",          ""},
                   {"description",   "bla bla bla"},
                   {"cost_to_enter", "1"}};
    GoalCreateController *rg = new GoalCreateController(new IDb);
    mapApi response = rg->execute(request);
    ASSERT_EQ(response.find("error")->second, "true");
    ASSERT_EQ(response.find("status")->second, "no_name");
    ASSERT_NE(atoi(response.find("goal_id")->second.c_str()), 1);
}

TEST(goal_create_controller, make_goal_2) {
    mapApi request{{"name",          ""},
                   {"description",   "bla bla bla"},
                   {"cost_to_enter", "1"}};
    GoalCreateController *rg = new GoalCreateController(new IDb);
    mapApi response1 = rg->execute(request);
    mapApi response2 = rg->execute(request);
    ASSERT_EQ(response2.find("error")->second, "true");
    ASSERT_EQ(response2.find("status")->second, "no_name");
    ASSERT_NE(atoi(response2.find("goal_id")->second.c_str()), 2);
}