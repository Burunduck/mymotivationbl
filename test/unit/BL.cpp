#include "gtest/gtest.h"
#include "BL.h"
#include "map"
#include "iostream"

using mapApi = std::map<std::string, std::string>;

TEST(bl_constructor, 1) {
    std::string command("register");
    BL* bl = new BL();
    bl->SetState(command);
    ASSERT_NE(bl, nullptr);
}

TEST(bl_constructor, 2) {
    std::string command = "goal_create";
    BL* bl = new BL();
    bl->SetState(command);
    ASSERT_NE(bl, nullptr);
}

TEST(bl_execute, register_test) {
    mapApi request {{"name", "Dima"},
                    {"lastname", "Alesha"},
                    {"password", "12345678"}};
    std::string command = "register";
    BL* bl = new BL();
    bl->SetState(command);
    mapApi response = bl->Execute(request);
    ASSERT_NE(response.find("session")->second, "");
    ASSERT_EQ(response.find("status")->second, "ok");
    ASSERT_EQ(response.find("user_id")->second, "1");
}

TEST(bl_execute, goal_create_test) {
    mapApi request {{"token", "12345678"},
                    {"goal_id", "1"},
                    {"goal_name", "PROJECT"},
                    {"goal_descr", "blabla"}};
    std::string command = "goal_create";
    BL* bl = new BL();
    bl->SetState(command);
    mapApi response = bl->Execute(request);
    ASSERT_EQ(response.find("error")->second, "false");
    ASSERT_EQ(response.find("goal_id")->second, "1");
}

TEST(bl_execute, send_report_test) {
    mapApi request {{"token", "12345678"},
                    {"user_id", "1"}};
    std::string command = "send_report";
    BL* bl = new BL();
    bl->SetState(command);
    mapApi response = bl->Execute(request);
    ASSERT_EQ(response.find("error")->second, "false");
    ASSERT_EQ(response.find("report_review")->second, "false");
}

TEST(bl_execute, close_goal_test) {
    mapApi request {{"time_now", "get_time"}};
    std::string command = "close_goal";
    BL* bl = new BL();
    bl->SetState(command);
    mapApi response = bl->Execute(request);
    ASSERT_EQ(response.find("report_review")->second, "false");
}

TEST(bl_execute, error_close_goal_test) {
    mapApi request {{"skdlsk", "dkfldkf"}};
    std::string command = "close_goal";
    BL* bl = new BL();
    bl->SetState(command);
    mapApi response = bl->Execute(request);
    ASSERT_NE(response.find("error")->second, "true");
}

TEST(bl_execute, error_login) {
    mapApi request {{"lOgIN", "dkfldkf"}};
    std::string command = "close_goal";
    BL* bl = new BL();
    bl->SetState(command);
    mapApi response = bl->Execute(request);
    ASSERT_NE(response.find("error")->second, "false");
}