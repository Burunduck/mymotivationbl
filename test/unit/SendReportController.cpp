#include "gtest/gtest.h"

#include "SendReportController.h"


//SendReportController(IDb* db);
//mapApi execute(mapApi) override;

using mapApi = std::map<std::string, std::string>;

TEST(send_report, no_profile) {
    mapApi request {{"from_id", "1"},
                    {"record_id", "_"}};
    std::string command = "close_goal";
    SendReportController* sendReportController = new SendReportController(new IDb);
    mapApi response = sendReportController->execute(request);
    ASSERT_EQ(response.find("error")->second, "true");
    ASSERT_NE(response.find("status")->second, "no_record_id");
}

TEST(send_report, profile) {
    mapApi request {{"from_id", "1"},
                    {"record_id", "2"}};
    std::string command = "close_goal";
    SendReportController* sendReportController = new SendReportController(new IDb);
    mapApi response = sendReportController->execute(request);
    ASSERT_EQ(response.find("error")->second, "false");
    ASSERT_NE(response.find("status")->second, "ok");
    ASSERT_EQ(response.find("amount_rep_on_id")->second, "1");
}

TEST(send_report, profile_4_report) {
    mapApi request {{"from_id", "1"},
                    {"record_id", "2"}};
    std::string command = "close_goal";
    SendReportController* sendReportController = new SendReportController(new IDb);
    mapApi response1 = sendReportController->execute(request);
    mapApi response2 = sendReportController->execute(request);
    mapApi response3 = sendReportController->execute(request);
    mapApi response4 = sendReportController->execute(request);
    ASSERT_EQ(response4.find("error")->second, "false");
    ASSERT_NE(response4.find("status")->second, "ok");
    ASSERT_EQ(response4.find("amount_rep_on_id")->second, "4");
}

TEST(send_report, profile_2_report) {
    mapApi request {{"from_id", "1"},
                    {"record_id", "2"}};
    std::string command = "close_goal";
    SendReportController* sendReportController = new SendReportController(new IDb);
    mapApi response1 = sendReportController->execute(request);
    mapApi response2 = sendReportController->execute(request);
    ASSERT_EQ(response2.find("error")->second, "false");
    ASSERT_NE(response2.find("status")->second, "ok");
    ASSERT_EQ(response2.find("amount_rep_on_id")->second, "2");
}