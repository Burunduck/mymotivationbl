#include "RegisterController.h"
#include "CloseGoalController.h"
#include "BL.h"
#include "gmock/gmock.h"
#include "IValidator.h"
#include "IDb.h"

class IDbMock: public IDb {
public:
    virtual ~IDbMock() {};
    MOCK_METHOD0(Create, int());
    MOCK_METHOD0(Update, int());
    MOCK_METHOD0(Delete, int());
    MOCK_METHOD0(Select, int());
};

using ::testing::AtLeast;
using ::testing::Return;
TEST(DbTest, Create) {
    IDbMock db;
    RegisterController registerController(&db);
    mapApi request{{"id","1"}};
    EXPECT_CALL(db, Create())
            .Times(AtLeast(1));
    registerController.execute(mapApi());
}

TEST(DbTest, Delete) {
    IDbMock db;
    CloseGoalController closeGoalController(&db);
    mapApi request{{"id","1"}};
    EXPECT_CALL(db, Delete())
            .Times(AtLeast(1));
    closeGoalController.execute(mapApi());
}