#include <IController.h>
#include "BL.h"
#include "gmock/gmock.h"

class ControllerMock: public IController {
public:
    virtual ~ControllerMock() {};
    MOCK_METHOD1(execute, mapApi (mapApi request));
};

using ::testing::AtLeast;
using ::testing::Return;
TEST(ControllerTest, execute) {
    ControllerMock controller;
    IValidator validator;
    mapApi request{{"id","1"}};
    EXPECT_CALL(controller, execute(request))
            .Times(AtLeast(1));;
    BL bl;
    bl.SetValidator(&validator);
    bl.SetController(&controller);
    bl.Execute(request);
}
