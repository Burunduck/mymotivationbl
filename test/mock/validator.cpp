#include <IController.h>
#include "BL.h"
#include "gmock/gmock.h"
#include "IValidator.h"
class ValidatorMock: public IValidator {
public:
    virtual ~ValidatorMock() {};
    MOCK_METHOD1(Handle, mapApi (mapApi request));
};

using ::testing::AtLeast;
using ::testing::Return;
TEST(ValidatorTest, Handle) {
    IController controller;
    ValidatorMock validator;
    mapApi request{{"id","1"}};
    EXPECT_CALL(validator, Handle(request))
            .Times(AtLeast(1));
    BL bl;
    bl.SetValidator(&validator);
    bl.SetController(&controller);
    bl.Execute(request);
}
