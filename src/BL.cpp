//
// Created by burunduck on 10.11.2020.
//

#include "../inc/BL.h"
#include "../Controlers/inc/RegisterController.h"
#include "../Controlers/inc/GoalCreateController.h"
#include "../Controlers/inc/SendReportController.h"
#include "../Controlers/inc/CloseGoalController.h"

constexpr unsigned int str2int(const char* str, int h = 0)
{
    return !str[h] ? 5381 : (str2int(str, h+1) * 33) ^ str[h];
}


int BL::SetState(std::string command) {
    ChooseControllers(command);
}

void BL::ChooseControllers(const std::string& command) {
    switch (str2int(command.c_str())) {
        case (str2int("register")):
//            validator = new RegisterValidator();
            controller = new RegisterController(proxyDb);
            break;
        case (str2int("goal_create")):
//            validator = new GoalCreateValidator();
            controller = new GoalCreateController(proxyDb);
            break;
        case (str2int("send_report")):
//            validator = new SendReportValidator();
            controller = new SendReportController(proxyDb);
            break;
        case (str2int("close_goal")):
//            validator = new CloseGoalValidator();
            controller = new CloseGoalController(proxyDb);
    }
}

mapApi BL::Execute(mapApi params) {
    mapApi response = controller->execute(params);
    response = validator->Handle(params);
    return params;
}

BL::BL() {

}

int BL::SetController(IController* controller) {
    this->controller = controller;
    return 0;
}

int BL::SetValidator(IValidator * validator) {
    this->validator = validator;
    return 0;
}

