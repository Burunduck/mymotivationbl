//
// Created by burunduck on 10.11.2020.
//

#include "../inc/IController.h"

IController::IController(IDb* proxyDb) {
    this->proxyDb = proxyDb;
}

IController::IController() {
}

mapApi IController::execute(mapApi) {
    return mapApi();
}

IController::~IController() {

}
