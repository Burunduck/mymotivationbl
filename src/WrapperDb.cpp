//
// Created by burunduck on 10.11.2020.
//
#include "../inc/WrapperDb.h"

WrapperDb::WrapperDb()
{

}

WrapperDb::WrapperDb(Model model, mapApi parameters) :
        _model{model}, _parameters{parameters}
{

}

void WrapperDb::setWrap(Model obj, mapApi parameters)
{
    _model = obj;
    _parameters = parameters;
}

Model WrapperDb::getModel()
{
    return _model;
}

mapApi WrapperDb::getParameters()
{
    return _parameters;
}