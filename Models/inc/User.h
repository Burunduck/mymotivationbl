//
// Created by burunduck on 10.11.2020.
//

#ifndef BL_USER_H
#define BL_USER_H
#include <string>
#include <map>

using mapApi = std::map<std::string, std::string>;

class User {
private:
    std::string login;
    std::string password;
    std::string email;
    std::string name;
    std::string lastname;
public:
    User();
    User(mapApi);
};


#endif //BL_USER_H
