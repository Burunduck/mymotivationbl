//
// Created by burunduck on 10.11.2020.
//

#ifndef BL_GOAL_H
#define BL_GOAL_H
#include <map>
#include <vector>
#include <string>
#include <time.h>


using mapApi = std::map<std::string, std::string>;

class Goal {
private:
    enum GoalState{
        USER,
        GOAL,
        REPORT,
        RECORD,
        MODERATOR,
    };

    int id;
    std::string name;
    int costToEnter;
    int balance;//Balance
    std::string description;
    std::vector<int> idRecords;//vector records
    int dateStart;
    int dateEnd;
    GoalState state;
public:
    Goal();
    Goal(mapApi);
};


#endif //BL_GOAL_H
