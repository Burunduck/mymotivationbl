//
// Created by burunduck on 10.11.2020.
//

#ifndef BL_REGISTERCONTROLLER_H
#define BL_REGISTERCONTROLLER_H

#include "IController.h"
#include "IDb.h"


class RegisterController : public IController {
    IDb* db;
public:
    int HashPassword(std::string password);
    RegisterController();
    RegisterController(IDb* db);
    mapApi execute(mapApi) override;
};


#endif //BL_REGISTERCONTROLLER_H
