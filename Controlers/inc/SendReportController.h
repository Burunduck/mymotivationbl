//
// Created by burunduck on 10.11.2020.
//

#ifndef BL_SENDREPORTCONTROLLER_H
#define BL_SENDREPORTCONTROLLER_H


#include "IController.h"
#include "IDb.h"

class SendReportController : public IController {
public:
    SendReportController(IDb* db);
    mapApi execute(mapApi) override;
};


#endif //BL_SENDREPORTCONTROLLER_H
