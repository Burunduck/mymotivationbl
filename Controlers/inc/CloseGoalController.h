//
// Created by burunduck on 10.11.2020.
//

#ifndef BL_CLOSEGOALCONTROLLER_H
#define BL_CLOSEGOALCONTROLLER_H


#include "IController.h"
#include "IDb.h"

class CloseGoalController : public IController {
public:
    CloseGoalController(IDb* db);
    mapApi execute(mapApi) override;
};
#endif //BL_CLOSEGOALCONTROLLER_H
