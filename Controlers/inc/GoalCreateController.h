//
// Created by burunduck on 10.11.2020.
//

#ifndef BL_GOALCREATECONTROLLER_H
#define BL_GOALCREATECONTROLLER_H


#include "IController.h"
#include "IDb.h"

class GoalCreateController : public IController {
public:
    GoalCreateController(IDb* db);
    mapApi execute(mapApi) override;
};


#endif //BL_GOALCREATECONTROLLER_H
