//
// Created by burunduck on 10.11.2020.
//

#include "../inc/RegisterController.h"
#include "../../Models/inc/User.h"

mapApi RegisterController::execute(mapApi request) {
    mapApi response;
    db->Create();
    //TODO
    return response;
}

RegisterController::RegisterController(IDb *db) {
    this->db= db;
}

RegisterController::RegisterController() {}

constexpr unsigned int str2int(const char* str, int h = 0)
{
    return !str[h] ? 5381 : (str2int(str, h+1) * 33) ^ str[h];
}

int RegisterController::HashPassword(std::string password) {
    return str2int(password.c_str());
}
