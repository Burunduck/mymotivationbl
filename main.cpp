#include <iostream>
#include <string>
#include <map>
#include <BL.h>

using namespace std;

using mapApi = std::map<std::string, std::string>;

int main() {
    mapApi fromParser = {{ "command", "register" }};
    string command = fromParser.find("command")->second;
    cout << command;
    BL* bl = new BL();
    bl->SetState(command);
    return 0;
}
